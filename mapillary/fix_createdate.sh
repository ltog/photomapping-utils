#!/bin/bash

# Copyright 2017 Lukas Toggenburger; https://github.com/ltog/mapillary_utils
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Show tags: exiftool -EXIF:DateTimeOriginal -EXIF:CreateDate "$file"

if [ $# -lt 1 ]; then
	echo "This software does the following changes to JPEG files:"
	echo "- overwrite CreateDate with value from DateTimeOriginal"
	echo
	echo "Usage: $0 file1 [file2 [file3 [...]]] or $0 *.jpg"
	exit 1
fi

parallel_options="--noswap --jobs 200%"

overwrite_createdate() {
	local file="$1"

	if [ ! -f $file ]; then
		echo WARNING: $file is not a file. Skipping...
		return
	fi

	exiftool '-EXIF:CreateDate<EXIF:DateTimeOriginal' -overwrite_original "$file"
}
export -f overwrite_createdate

parallel $parallel_options overwrite_createdate ::: "$@"

