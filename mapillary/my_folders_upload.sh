#!/bin/bash

# Copyright 2015 Lukas Toggenburger; https://github.com/ltog/mapillary_utils
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

parallel_options="--noswap"

check_geotagging() {
	local file="$1"

	local exifs=$(exiftool "$file")

	local img_direction=$(echo "$exifs" | grep "^GPS Img Direction * : ")
	local orientation=$(echo "$exifs" | grep "^Orientation * : ")
	local gps_lat=$(echo "$exifs" | grep "^GPS Latitude * : ")
	local gps_lon=$(echo "$exifs" | grep "^GPS Longitude * : ")
	local gps_pos=$(echo "$exifs" | grep "^GPS Position * : ")

	if [ -z "$img_direction" ] || [ -z "$orientation" ] || [ -z "$gps_lat" ] || [ -z "$gps_lon" ] || [ -z "$gps_pos" ]; then
		echo "ERROR: Could not find valid geotagging in file \"$file\" (and maybe others)."
		return 1
	else
		echo "Geotagging of $file seems ok."
		return 0
	fi
}
export -f check_geotagging

if [ $# -ne 1 ]; then
	cat <<-USAGE
Usage: $0 DIRECTORY_CONTAINING_SUBDIRECTORIES

This script will search for subdirectories in the given directory. Pictures of each contained subdirectory will be uploaded as a sequence to Mapillary therefore generating (possibly) multiple sequences. The file do_authentication_exports.sh must export the variables MAPILLLARY_SIGNATURE_HASH, MAPILLARY_PERMISSION_HASH and MAPILLARY_USERNAME as described in http://blog.mapillary.com/technology/2014/07/21/upload-scripts.html .

Before uploading all files will be checked for the existence of geotagging (by searching for the viewing direction). Files will only be uploaded if ALL pictures contain it.

Directory structures with more than two levels of directories (i.e. more than directory-subdirectory-file) will not be processed, instead an error message will be displayed and this software be aborted.
USAGE
	exit 1
fi

source ./do_authentication_exports.sh || exit 1 # script must be run in this file's directory

dir="$1"
if [ ! -d "$dir" ]; then
	echo "ERROR: "$dir" is not a directory."
	exit 1
fi

if [ "$(find "$dir" -mindepth 2 -type d | wc -l)" -ne 0 ]; then
	echo "ERROR: Subdirectories of $dir are too nested"
	exit 1
fi

echo "Going to check images in all subdirectories for valid geotagging..."

find "$dir" -mindepth 1 `#-maxdepth 1` -type d -print0 | xargs -0 -I{.} find {.} -maxdepth 1 -type f | sort | parallel $parallel_options --halt 2 check_geotagging {}

# nested find from http://unix.stackexchange.com/a/18101 ; Alternative: http://stackoverflow.com/a/17105398
# evaluation of return values and early termination of parallel jobs: https://www.gnu.org/software/parallel/parallel_tutorial.html#Termination
# support for many arguments: http://stackoverflow.com/a/23121083 WARNING: won't work with files with newline in name!
# writing test files with newlines in name: https://unix.stackexchange.com/questions/212688/add-a-newline-into-a-filename-with-mv
# -mindepth 1 : don't return base folder (alternative: use '!', see http://stackoverflow.com/a/17389439 )
# -maxdepth 1 : don't look at subdirectories of subdirectories (since I didn't test upload_with_authentication.py with this case, better skip this and check possibly too much)

if [ $? -ne 0 ]; then # http://stackoverflow.com/q/5195607
	echo "Nothing was uploaded. Aborting..."
	exit 1
fi

for subdir in "$dir"/*; do
	if [ -d "$subdir" ]; then
		echo "Going to upload ${subdir}..."
		yes | python ./upload_with_authentication.py "$subdir"
	fi
done
