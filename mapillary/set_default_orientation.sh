#!/bin/bash

# Copyright 2017 Lukas Toggenburger; https://github.com/ltog/photomapping-utils
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ $# -lt 1 ]; then
	echo "This software does set the EXIF orientation tag to the value 1. This value has no effect compared to no orientation tag at all, but is required for pictures to be uploaded to Mapillary. The script works recursively in the given folder and all subfolders."
	echo
	echo "Usage: $0 /path/to/pics"
	exit 1
fi

# from https://superuser.com/a/594052 , https://stackoverflow.com/a/4824637
# "-wm cg": don't change existing tags, see http://u88.n24.queensu.ca/exiftool/forum/index.php?topic=6583.0

find "$1" -type f -regextype posix-extended -iregex ".+\.jpe?g$" | sort | parallel --noswap -m exiftool -wm cg -Orientation=1 -n -overwrite_original
