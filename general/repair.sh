#!/bin/bash

# Copyright 2017 Lukas Toggenburger; https://github.com/ltog/photomapping-utils
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


if [ $# -lt 1 ]; then
	echo "This software tries to repair errors in EXIF data in JPEG files. The script works recursively in the given folder and all subfolders."
	echo
	echo "Usage: $0 /path/to/pics"
	exit 1
fi

# source: https://www.sno.phy.queensu.ca/~phil/exiftool/faq.html#Q20

find "$1" -type f -regextype posix-extended -iregex ".+\.jpe?g$" | sort | parallel --noswap -m exiftool -overwrite_original -all= -tagsfromfile @ -all:all -unsafe -icc_profile

