#!/bin/bash

# TODO: move away pictures without geotagging

if [ $# -lt 1 ]; then
	echo "This software does recursively the following changes to JPEG files:"
	echo "- remove all JPEG meta-information (including thumbnail) except EXIF-tags"
	echo "- add the GPSVersionID=2.3.0.0 EXIF tag"
	echo
	echo "Usage: $0 /path/to/pics"
	exit 1
fi

find "$1" -type f -regextype posix-extended -iregex ".+\.jpe?g$" | sort | parallel --noswap -m jhead -dc -di -dx -du # remove all side-information except exif
find "$1" -type f -regextype posix-extended -iregex ".+\.jpe?g$" | sort | parallel --noswap -m exiftool \
		-overwrite_original    `# do not create a backup file` \
		-GPSVersionID=2.3.0.0  `# set gps version id so the mapillary webinterface can process the pictures` \
		-ImageDescription=     `# remove image description (created e.g. by Mapillary app)` \
		-ifd1:all=             `# remove thumbnail and associated tags ( http://perlmaven.com/how-to-remove-thumbnail-from-a-jpeg-using-image-exiftool )`
