#!/bin/bash

if [ $# -ne 1 ]; then
	cat <<-USAGE
Usage: $0 DIRECTORY

This script will rename image files in the given directory and all subdirectories according to their creation date (specified by EXIF tag "Date/Time Original"). As a consequence, the alphabetical order corresponds to the chronological order. The script also processes subsecond values. If multiple files have an identical creation date and time, a suffix will be appended to their name.
USAGE
	exit 1
fi

if [ ! -d "$1" ]; then
	echo "ERROR: "$1" is not a directory. Aborting without any changes..."
	exit 1
fi

# according to http://u88.n24.queensu.ca/exiftool/forum/index.php/topic,2736.msg43653.html#msg43653
exiftool -r -d %Y-%m-%d_%H%M%S '-filename<${datetimeoriginal}.${subsectimeoriginal;$_.=0 x(3-length)}%-c.%le' "$1"
