#!/usr/bin/env python

# taken from https://github.com/mapillary/mapillary_tools
 
# The MIT License (MIT)
# 
# Copyright (c) 2014 Mapillary AB
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import argparse
from lib.sequence import Sequence
from lib.exifedit import ExifEdit

'''
Interpolates the direction of an image based on the coordinates stored in
the EXIF tag of the next image in a set of consecutive images.

Uses the capture time in EXIF and looks up an interpolated lat, lon, bearing
for each image, and writes the values to the EXIF of the image.

An offset angele relative to the direction of movement may be given as an optional
argument to compensate for a sidelooking camera. This angle should be positive for
clockwise offset. eg. 90 for a rightlooking camera and 270 (or -90) for a left looking camera

@author: mprins
@license: MIT
'''

def write_direction_to_image(filename, direction):
    '''
    Write the direction to the exif tag of the photograph.
    @param filename: photograph filename
    @param direction: direction of view in degrees
    '''
    exif = ExifEdit(filename)
    try:
        exif.add_direction(direction, precision=10)
        exif.write()
        print("Added direction to: {0} ({1} degrees)".format(filename, float(direction)))
    except ValueError, e:
        print("Skipping {0}: {1}".format(filename, e))

def get_args():
    parser = argparse.ArgumentParser(description='Interpolate direction given GPS positions')
    parser.add_argument('path', help='path to your photos')
    parser.add_argument('--offset_angle',
        type=float, help='offset angle relative to camera position', default=0.0)
    args = parser.parse_args()
    return args

if __name__ == '__main__':

    args = get_args()
    path = args.path

    # offset angle, relative to camera position, clockwise is positive
    offset_angle = args.offset_angle

    s = Sequence(path)
    bearings = s.interpolate_direction(offset_angle)
    for image_name, bearing in bearings.iteritems():
        print(image_name)
        write_direction_to_image(image_name, bearing)

