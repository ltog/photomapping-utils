#!/usr/bin/python3

"""
Copyright 2019 Lukas Toggenburger; http://gitlab.com/ltog/photomapping-utils

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import argparse
import lxml.etree as etree
import datetime
import dateutil
import dateutil.parser

filename_suffix = "_fixed"

def main(args):

    # get an iterable
    context = etree.iterparse(args.infile, events=("start", "end"))

    # turn iterable into an interator
    context = iter(context)

    # get the root element
    event, root = next(context)

    count = 0

    outfile = args.outfile
    if args.outfile is None:
        base, ext = os.path.splitext(args.infile) # https://stackoverflow.com/a/678242
        outfile = base + filename_suffix + ext
    if os.path.exists(outfile):
        print("The file " + outfile + " exists. Aborting...") 
        sys.exit(1)

    for event, el in context:
        if (event == "end" and (el.tag == "{http://www.topografix.com/GPX/1/0}time" or el.tag == "{http://www.topografix.com/GPX/1/1}time")):
            date = el.text
            if date < "2019-04-06T23:59:42Z":
                count += 1
                el.text = get_fixed_date(date)

    out = open(outfile, 'wb')
    out.write(etree.tostring(root, pretty_print=True))
    out.close()

    print(str(count) + " dates have been fixed.")

def get_fixed_date(date):
    parsed_date = dateutil.parser.parse(date)
    fixed_date = parsed_date + datetime.timedelta(weeks=1024)
    return fixed_date.isoformat()[:-6]+'Z' # remove the last 6 characters '+00:00' and append Z instead

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This script will change dates in GPX files whose dates wrongfully lie in the second GPS time epoch (approx. 1999 to 2019) but should be in the third epoch (2019 or later). If you don't specify a filename, a new file with the suffix '" + filename_suffix + "' will be created.")
    parser.add_argument("-i", "--infile", type=str, help="The file to be read", required=True)
    parser.add_argument("-o", "--outfile", type=str, help="The file to be written", required=False)

    args = parser.parse_args()

    main(args)
