#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Usage: $0 directory_with_pics gpx_outfile"
	exit 1
fi

if [ ! -d "$1" ]; then
	echo "ERROR: $1 is not a directory."
	exit 1
fi

if [ -e $2 ]; then
	echo "ERROR: $2 exists."
	exit 1
fi

indir="$1"
outfile="$2"

# http://stackoverflow.com/a/246128
scriptdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# from http://www.sno.phy.queensu.ca/~phil/exiftool/geotag.html#Inverse
exiftool \
	-r                        `# recursively process files` \
	-if '$gpsdatetime'        `# process only files containing gps date/time` \
	-if '$gpslatitude'        `# process only files containing gps latitude` \
	-if '$gpslongitude'       `# process only files containing gps longitude` \
	-fileOrder gpsdatetime    `# sort gpx tracks points by gps time/date` \
	-p "${scriptdir}"/gpx.fmt `# use this file as template` \
	-d %Y-%m-%dT%H:%M:%SZ     `# ???` \
	"$indir" > "$outfile"

	
