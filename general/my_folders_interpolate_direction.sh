#!/bin/bash


# Copyright 2015 Lukas Toggenburger; https://github.com/ltog/mapillary_utils
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ $# -ne 2 ]; then
	cat <<-USAGE
Usage: $0 DIRECTORY_CONTAINING_SUBDIRECTORIES CAMERA_DIRECTION

This script will search for subdirectories in the given directory. Pictures of each contained subdirectory will be given an interpolated viewing directory, based on the previous and next picture.

Right viewing: 90
Left viewing: 270

For directory structures with more than two levels of directories (i.e. more than directory-subdirectory-file) the behaviour was not tested.
USAGE
	exit 1
fi

parallel_options="--noswap"

interpolate() {
	local subdir="$1"
	local angle="$2"

	if [ -d "$subdir" ]; then
		echo "Going to interpolate viewing directions in ${subdir} with camera angle $angle..."
		python ./interpolate_direction.py --offset_angle "$angle" "$subdir"
	fi
}
export -f interpolate

dir="$1"
if [ ! -d "$dir" ]; then
	echo "ERROR: "$dir" is not a directory."
	exit 1
fi

angle="$2"
if [[ "$angle" -lt "-180" || "$angle" -gt "359" ]]; then
	echo "ERROR: "$angle" is not in the interval -180..359"
	exit 1
fi

# TODO: check geotagging

parallel $parallel_options interpolate {1} "$angle" ::: "$dir"/*
