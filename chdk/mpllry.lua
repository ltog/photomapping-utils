--[[
Copyright 2015, 2017 Lukas Toggenburger; https://github.com/ltog/mapillary_utils

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

--[[
@title Mapillary Intervalometer
--]]

--[[
Description: This software is to be run on a CHDK-enabled camera. It was tested on a Canon PowerShot S100.

Features:

- Takes pictures as fast as possible (note: speed is reduced if picture review is enabled)
- Fixed focus distance for short shooting intervals
- Don't take picture if environment is dark (e.g. in a tunnel)
- Disable flash and AF assist light during script operation (this is done in order to not annoy/endanger other traffic participants)
- Toggle display by holding "DISP." button (to save battery power)
- Zoom in/out during script operation (no need to stop/restart the script when you want to change zoom settings)
--]]

-- User-defined values go here -------------------------------------------------

fixed_focus_distance = 10000 -- distance in mm: valid range: 0 to 65535
bv96_dark_limit = 100 -- what bv96 value is considered too dark to take pictures
repeat_sleep_duration = 20 -- how long to sleep in repeat-loops that check for a certain condition

-- No more user-defined values after here --------------------------------------

props = require("propcase") -- load a table that maps camera property case names to numbers. this makes it more portable between different cameras

flash_mode = get_prop(props.FLASH_MODE) -- read the initial mode of the flash
af_assist_beam = get_prop(props.AF_ASSIST_BEAM) -- read the initial state of the af assist beam

my_display_state = 1
function my_set_display(arg)
	set_lcd_display(arg)
	sleep(50)
	set_backlight(arg) -- not clear if necessary, see http://forum.chdk-treff.de/viewtopic.php?f=7&t=3424#p30141
	sleep(50)
	my_display_state = arg
end

function my_toggle_display()
	my_set_display((my_display_state + 1) % 2)
end

-- This function gets called when the script is stopped using the trigger. It should be placed as early in the script as possible.
function restore()
	set_prop(props.FLASH_MODE, flash_mode)
	set_prop(props.AF_ASSIST_BEAM, af_assist_beam)
	my_set_display(1)
	set_mf(0)
	--set_aflock(0) -- should work too, untested
end

function init()
	set_prop(props.FLASH_MODE, 2) -- disable flash (0=Auto, 1=ON, 2=OFF)
	set_prop(props.AF_ASSIST_BEAM, 0) -- disable beam (0=OFF, 1=ON)
	set_console_layout(0,0,45,12)
	set_mf(1)
	--set_aflock(1) -- should work too, untested
	
	set_focus(fixed_focus_distance)
        --print("focus0 mode="..get_focus_mode().." state="..get_focus_state().." ok="..get_focus_ok())
	sleep(500)
end

init()

repeat
	press("shoot_half") -- start measuring (auto focus, etc.)
	repeat sleep(repeat_sleep_duration) until get_shooting() == true -- wait for measurements

	if (get_bv96() > bv96_dark_limit) then
		is_dark = false
	else
		is_dark = true
	end

	if is_dark == false then
		exp_count=get_exp_count() -- read current picture number
		press("shoot_full_only") -- take the picture
		repeat sleep(repeat_sleep_duration) until(get_exp_count() ~= exp_count) -- wait until picture is taken
		release("shoot_full_only")
		if (get_bv96() < bv96_dark_limit+200) then
			print("dark but still shooting; bv96="..get_bv96())
		end
	else -- is_dark == true
		print("too dark; bv96="..get_bv96())
		sleep(2000)
	end

	release("shoot_half")
	repeat sleep(repeat_sleep_duration) until get_shooting() == false -- wait for `release("shoot_half")`

	-- handle display on/off
	if(is_pressed("down")) then -- button "display" is not recognized on S100
		my_toggle_display()
	end

	-- toggle picture review
	-- if (is_pressed("up")) then
	-- 	print("'up' was pressed")
	-- 	print("PC216=" .. get_prop(216))
	-- end

	-- handle zoom
	while (is_pressed("zoom_in") or is_pressed("zoom_out")) do
		while(is_pressed("zoom_in") or is_pressed("zoom_out")) do
			if(is_pressed("zoom_in")) then
				set_zoom_rel(2)  -- S100 only accepts step sizes > 1 ?
			elseif (is_pressed("zoom_out")) then
				set_zoom_rel(-2) -- S100 only accepts step sizes > 1 ?
			end
			sleep(90) -- slow down zooming so the operator has time to release the zoom buttons without the zoom overshooting
		end
		sleep(700) -- make it possible to switch zoom direction and stay in the most outer while-loop without taking another picture
	end

	-- handle ring function
	-- if (is_pressed("erase")) then
	-- 	print("'erase' was pressed")
	-- 	click("wheel_left")
	-- 	sleep(100)
	-- end
	-- if (is_pressed("video")) then
	-- 	print("'video' was pressed")
	-- 	click("wheel_right")
	-- 	sleep(100)
	-- end

until (false)
