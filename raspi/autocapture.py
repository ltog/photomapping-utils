#!/usr/bin/python3

# Copyright 2017 Lukas Toggenburger; https://github.com/ltog/mapillary_utils
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from picamera import PiCamera
from time import sleep
import datetime
import os
import shutil

def main():
    destination_dir = "/home/pi/Desktop/autocapture/"

    out_dir = destination_dir + "/" + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S.%f") + "/"

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    camera = PiCamera()
    camera.resolution = (3280, 2464)
    camera.exposure_mode = "sports"
    camera.awb_mode = "sunlight"
    camera.exposure_compensation = 5 # lighten pictures up a bit
    camera.exif_tags['IFD0.Orientation'] = "1" # 1=default orientation, needed for Mapillary

    count = 0

    set_exif(camera) # needed for the first picture

    for filename in camera.capture_continuous(out_dir + "/" + 'img{counter:06d}.jpg', thumbnail=None, quality=18):
        print('Captured %s' % filename)
        sleep(0.6)
        count += 1

        if shutil.disk_usage(out_dir).free < 40000000 : # values in bytes
            break

        set_exif(camera)


def set_exif(camera):
    now = datetime.datetime.now()

    subsec_string = get_subsec_string(now)
    camera.exif_tags['EXIF.SubSecTime']          = subsec_string
    camera.exif_tags['EXIF.SubSecTimeOriginal']  = subsec_string
    camera.exif_tags['EXIF.SubSecTimeDigitized'] = subsec_string

    datetime_string = get_datetime_string(now)
    camera.exif_tags['IFD0.DateTime']          = datetime_string
    camera.exif_tags['EXIF.DateTimeOriginal']  = datetime_string
    camera.exif_tags['EXIF.DateTimeDigitized'] = datetime_string

def get_subsec_string(now):
    return str(now.microsecond)[:-3].zfill(3)

def get_datetime_string(now):
    return now.strftime("%Y:%m:%d %H:%M:%S")

if __name__ == "__main__":
    main()
