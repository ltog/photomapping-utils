#!/bin/bash

# Copyright 2016 Lukas Toggenburger; https://github.com/ltog/mapillary_utils
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

parallel_options="--noswap --keep-order --jobs 200%"

export limit=1.0     # the distance limit in meters

print_result() {
	local file="$1"
	# see https://www.imagemagick.org/Usage/quantize/#extract
	local ppm=$(convert "$file" -fuzz 20% -fill black +opaque "rgb(219,197,26)" -fill white -opaque "rgb(219,197,26)" -print "%[fx:floor(1000000*mean)]\n" null:)

	local focusdistanceupper=$(exiftool "$file" | grep -e "^Focus Distance Upper")
	local distance=$(echo "$focusdistanceupper" | sed -e "s/.*: \(.*\) m/\1/")

	if [ $(echo "$distance < $limit" | bc) -eq 1 ] || [ $(echo "$ppm > 10000" | bc) -eq 1 ]; then
		echo "$ppm:$distance:$file"
	fi

}
export -f print_result

if [ $# -ne 1 ]; then
	cat <<-USAGE
Usage: $0 DIRECTORY_TO_BE_PROCESSED

This script will scan the given directory and all subdirectories for JPEG images that seem to contain a yellow HOLUX GPS logger. Detection is based on the amount of yellow pixels and the focus distance.
USAGE
	exit 1
fi

dir="$1"
if [ ! -d "$dir" ]; then
	echo "ERROR: "$dir" is not a directory."
	exit 1
fi

find "$dir" -type f -iname "*.jpeg" -o -iname "*.jpg" -print0 | sort -z | parallel -d '\0' $parallel_options print_result :::
