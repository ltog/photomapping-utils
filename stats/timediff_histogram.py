#!/usr/bin/python3

"""
Copyright 2017 Lukas Toggenburger; https://github.com/ltog/photomapping-utils

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import os.path
from lxml import etree as ET
import re
import dateutil.parser
from collections import Counter
import pprint

def main(args):
    if not os.path.exists(args.inputfile):
        sys.exit("ERROR: File '" + args.inputfile + "' does not exist. Aborting...")

    tree = ET.parse(args.inputfile)

    timestamps = xml2list(tree)
    diffs = []

    for i in range(len(timestamps)-1):
        diffs.append(timestamps[i+1]-timestamps[i])

    hist = dict(Counter(diffs)) # https://stackoverflow.com/a/5829377

    prettyprint(hist)


def xml2list(tree):
    namespace = get_namespace(tree)

    mylist = []

    # iterate over each <trkpt>
    for el in tree.findall('.//{0}trkpt'.format(namespace)): # according to http://stackoverflow.com/a/1319417
        time  = el.find(".//{0}time".format(namespace)).text

        mylist.append(date2epoch(time))

    return sorted(mylist)


def prettyprint(stuff):
    pp = pprint.PrettyPrinter(indent=4, width=1)
    pp.pprint(stuff)


def date2epoch(date: str):
    return int(dateutil.parser.parse(date).strftime('%s'))


# PRE: assume only one namespace is in use
def get_namespace(tree):
    # see https://stackoverflow.com/questions/9513540/python-elementtree-get-the-namespace-string-of-an-element
    # see https://stackoverflow.com/a/1319575
    # see https://stackoverflow.com/a/20104763

    #namespace = tree.getroot().get("version") # crashes when looking for attribute 'xmlns', instead of 'version'

    re_match = re.match('\{.*\}', tree.getroot().tag)
    if re_match:
        return re_match.group(0)
    else:
        return ''


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Read a .gpx file and print a statistic of the occurring time differences between trackpoints")
    parser.add_argument("inputfile", help="the file to analyze")
    args = parser.parse_args()

    main(args)
