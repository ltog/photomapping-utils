# photomapping-utils

Utilities for mass-creating, -processing and -uploading geotagged photos

## Directories

- `chdk`: Software for usage on Canon cameras that support the CHDK software
- `general`: Vendor-agnostic software for processing images
- `mapillary`: Software in context with Mapillary
- `raspi`: Software to be run on Raspberry Pi
- `stats`: Software for generating statistics over images

## My workflow

- Start GPS logger(s)
- Take pictures
  - Canon S100: `chdk/mpllry.lua`
  - Raspberry Pi: `raspi/autocapture.py`
- Retreive pictures:
  - Canon S100: If there was an overflow in the filename's counter: `general/prepend_filename.txt`
- Retreive GPX data
  - Canon S100: `general/pics2gpx/create_gpx_from_pics.sh`
- Combine GPX tracks: `general/merge_gpx.py`
- Rasperry Pi: Repair pictures whose geotagging can't be displayed by JOSM: `general/repair.sh`
- Geotag pictures using JOSM
  - Canon S100: Determine offset statistically by looking at pictures and comparing them to aerial imagery
- Remove duplicate pictures that were automatically shot when standing still: `general/my_remove_duplicates.sh` (this will give an error when called for nested directories)
  - Raspberry Pi: To prevent silent failing of my_remove_duplicates.sh, call `general/repair.sh`
- Remove non-EXIF metainformation and thumbnails, add GPSVersionID=2.3.0.0: `general/remove_metainformation.sh`
- Optional: Split into multiple tracks: `general/time_split.py` or manually (in bash you can use the command `mv img00{0000..5126}.jpg a/` to move a range of images at once)
- Generate viewing direction: `general/my_folders_interpolate_direction.sh`
- Upload pictures:
  - Mapillary: `mapillary/my_upload.sh` or `mapillary/my_folders_upload.sh`
  - OpenStreetCam: Generally according to: <https://github.com/openstreetcam/upload-scripts>
    - `cd $checked_out_dir`
    - `source bin/activate`
    - Test: `find /home/bozo/mypics -mindepth 1 -type d`
    - `find /home/bozo/mypics -mindepth 1 -type d -print0 | xargs -L 1 --null ./upload_exif.py -p`

## time_split.py

Syntax:

    python time_split.py path/to/images [cutoff-time]

## interpolate_direction.py

Syntax:

    python interpolate_direction.py path/to/images direction_of_camera

Install requirements (Ubuntu):

    sudo apt-get install python-pyexiv2

## remove_duplicates.py

Syntax:

    remove_duplicates.py -d 2 /path/to/pics /path/to/dup_pics
